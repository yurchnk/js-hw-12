function checkKey(btn, key){
    btn.forEach(item => {
        item.style.backgroundColor  = '#000000'; 
        if (item.textContent === key)
            item.style.backgroundColor  = '#3e3bc8'; 
        })
}

let key = document.querySelectorAll('.btn');
document.addEventListener('keydown', function (event) {
    switch (event.code) {
        case 'Enter':
            checkKey(key, 'Enter');
            break;
        case 'KeyS':
            checkKey(key, 'S');
            break;
        case 'KeyE':
            checkKey(key, 'E');
            break;
        case 'KeyO':
            checkKey(key, 'O');
            break;
        case 'KeyN':
            checkKey(key, 'N');
            break;
        case 'KeyL':
            checkKey(key, 'L');
            break;
        case 'KeyZ':
            checkKey(key, 'Z');
            break;
        default:
            break;
    }
});